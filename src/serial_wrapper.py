# -*- coding: utf-8 -*-

# to install : 
# pip  uninstall  serial
# pip3 uninstall  serial
# pip  uninstall pyserial
# pip3 uninstall pyserial
# pip  install   pyserial
# pip3 install   pyserial


import os
import time

import importlib
from importlib.machinery import SourceFileLoader
from os.path   import join , dirname


dir_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
base_driver_path = dir_path

list_path = ["lib", 
            "base_driver", 
            "src", 
            "base_driver.py" 
           ]
for el in list_path:
    base_driver_path = os.path.join(base_driver_path, el)

base = SourceFileLoader(dir_path, base_driver_path).load_module()   

import serial
import serial.tools.list_ports as ports_list
import threading
from threading import BoundedSemaphore

import queue

class thread_rd(threading.Thread):
  def __init__(self, i_serial, i_queue):
    super(thread_rd, self).__init__()
    self.daemon = True
    self.port   = i_serial
    self.queue  = i_queue
    # self.sem    = Semaphore(0)
    self.EOL    = None # b'\n\r'
    self.buffer = b''
    self.timeout = None
    
  def run(self):
    print("serial thread read started")
    while True:
      char = self.port.read(1)
      
      if self.EOL == None: 
        self.queue.put(char)
      else:  
        self.buffer += char
        if self.EOL in self.buffer:
            # self.sem.release()
            self.buffer = self.buffer.replace(self.EOL, b'')
            self.queue.put(self.buffer)
            self.buffer = b''
      
  def set_rd_timeout(self, timeout = None):
    self.timeout = timeout

  def rd(self):
    msg = self.queue.get(timeout = self.timeout)
    return msg

  def set_EOL(self, i_eol):
    self.EOL = i_eol

  def reset_buffer(self):
    self.buffer = b''

class thread_wr(threading.Thread):
  def __init__(self, i_serial, i_queue):
    super(thread_wr, self).__init__()
    self.daemon = True
    self.port = i_serial
    self.queue = i_queue
  def run(self):
    print("serial thread write started")
    while True:
        # time.sleep(0.001)
        msg = self.queue.get()
        if isinstance(msg, type(b'byte')):
            self.port.write(msg)
        elif isinstance(msg, str):
            # for char in msg:
            char_encoded = msg.encode('ascii')
            self.port.write(char_encoded)
        time.sleep(0.02) 
            
  def wr(self, msg):
    self.queue.put(msg)

class handle(base.driver):
  def __init__(self, is_sim = False):
    super(handle, self).__init__()
    self.abs_path = os.path.dirname( os.path.abspath(__file__) )
    self._is_sim = is_sim
    self.name                 = "SERIAL PORT"

    self.port_serie           = serial.Serial()
    self.port_serie.baudrate  = 9600
    self.port_serie.port      = "None"
    self.port_serie.timeout   = None
    self.port_serie.parity    = serial.PARITY_NONE
    self.port_serie.bytesize  = serial.EIGHTBITS
    self.port_serie.stopbits  = serial.STOPBITS_ONE
    self.port_serie.rtscts    = False
    self.port_serie.xonxoff   = False
    self.port_serie.dsrdtr    = False
    self.serie_status = False
    # self.sem        = BoundedSemaphore(1)    
    self.queue_rd   = queue.SimpleQueue()
    self.queue_wr   = queue.SimpleQueue()
    self.status_rd  = False
    self.status_wr  = False
    self._thread_rd = thread_rd(self.port_serie, self.queue_rd)
    self._thread_wr = thread_wr(self.port_serie, self.queue_wr)
    self.set_time_per_byte(( 1 + 8 + 1) / self.port_serie.baudrate) 

  def set_time_per_byte(self, value):
    self.time_per_byte = value
  
  def get_time_per_byte(self):
    return self.time_per_byte
  
  def start_rd(self):
    if not(self.serie_status):
      self.serie_status = True
      self.port_serie.open()
    if not(self.status_rd):
      self._thread_rd.start()
      self.status_rd = True

  def start_wr(self):
    if not(self.serie_status):
      self.serie_status = True
      self.port_serie.open()
    if not(self.status_wr):
      self._thread_wr.start()
      self.status_wr = True
      
  def is_started(self):
    return self.serie_status
  
  def start(self):
    print("Start serial Wrapper")
    if self.port_serie.port != "None":
        self.start_rd()
        self.start_wr()
    
  def clear_rd(self):
    self._thread_rd.reset_buffer()
    while not self.queue_rd.empty():
      try:
          self.queue_rd.get(False)
      except Empty:
          continue
    self._thread_rd.reset_buffer()
  
  def clear_wr(self):
    while not self.queue_wr.empty():
      try:
          self.queue_wr.get(False)
      except Empty:
          continue
          
  def empty_rd(self):
    return self.queue_rd.empty()
    
  # def wr_join(self):
    # self.queue_wr.join()

  def empty_wr(self):
    return self.queue_wr.empty()
    
  def wr(self, msg):
    self._thread_wr.wr(msg)
  
  def rd(self, p_timeout = None):
    try:
      line = self._thread_rd.rd()
    except:
      line = None
    return line
    
  def set_EOL(self, i_eol):
    self._thread_rd.set_EOL(i_eol)
    
  def set_baudrate(self, value):
    self.port_serie.baudrate = value
    
  def set_port    (self, value):   
    self.port_serie.port = value   
    
  def set_timeout (self, value):
    self.port_serie.timeout = value   
    
  def set_parity  (self, value):  
    self.port_serie.parity = value 
    
  def set_bytesize(self, value):  
    self.port_serie.bytesize  
    
  def set_stopbits(self, value):    
    self.port_serie.stopbits = value
    
  def set_rtscts  (self, value):   
    self.port_serie.rtscts = value    
    
  def set_xonxoff (self, value):    
    self.port_serie.xonxoff = value   
    
  def set_dsrdtr  (self, value):   
    self.port_serie.dsrdtr = value 

  # def set_rd_EOF(self, i_eof):
    # self._thread_rd.set_EOF(i_eof)
  
  def set_rd_timeout(self, timeout = None):
    self._thread_rd.set_rd_timeout(timeout)
    
  def comports(self):
    return list(serial.tools.list_ports.comports())

if __name__ == '__main__':
    import sys
    port_list = []
    print("")
    for el in list(serial.tools.list_ports.comports()):
        print(el)
        port_list.append(el)
    print("")
    
  # if "COM4 - USB Serial Port (COM4)" in port_list:
    # handle.set_port("COM4")
  # elif "COM5 - USB Serial Port (COM5)  " in port_list:

    handle = handle()
    handle.start()
    
    if handle.is_started():
       print("test ok")  
    
    print("test ok")
  